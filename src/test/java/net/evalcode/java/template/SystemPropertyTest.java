package net.evalcode.java.template;


import static org.junit.Assert.assertEquals;
import net.evalcode.java.template.SystemProperty;
import org.junit.Test;


/**
 * Test {@link SystemProperty}
 *
 * @author carsten.schipke@gmail.com
 */
public class SystemPropertyTest
{
  // TESTS
  @Test
  public void testConsistency()
  {
    for(final SystemProperty systemProperty : SystemProperty.values())
    {
      assertEquals(System.getProperty(systemProperty.key()),
        systemProperty.rawValue()
      );

      assertEquals(System.getProperty(systemProperty.key(), systemProperty.defaultValue()),
        systemProperty.value()
      );
    }
  }
}
