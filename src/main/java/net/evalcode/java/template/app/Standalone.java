package net.evalcode.java.template.app;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Standalone
 *
 * <p> Standalone Application Entry Point
 *
 * @author carsten.schipke@gmail.com
 */
public final class Standalone
{
  // PREDEFINED PROPERTIES
  static final Logger LOG=LoggerFactory.getLogger(Standalone.class);
  static final ClassLoader BOOT_CLASS_LOADER=Thread.currentThread().getContextClassLoader();


  // CONSTRUCTION
  private Standalone()
  {
    super();
  }


  // STATIC ACCESSORS
  public static void main(final String[] args)
  {
    LOG.info("Application: {}.", Standalone.class.getName());

    (new Standalone()).start();
  }

  public static ClassLoader getBootClassLoader()
  {
    return BOOT_CLASS_LOADER;
  }


  // IMPLEMENTATION
  private void start()
  {
    Runtime.getRuntime().addShutdownHook(new Thread()
    {
      @Override
      public void run()
      {
        // TODO implement shutdown
        LOG.info("Shutdown.");
      }
    });

    // TODO implement startup
    LOG.info("Startup.");
  }
}
