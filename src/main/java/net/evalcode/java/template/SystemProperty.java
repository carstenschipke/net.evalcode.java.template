package net.evalcode.java.template;


import net.evalcode.java.template.app.Standalone;
import net.jcip.annotations.ThreadSafe;


/**
 * SystemProperty
 *
 * <p> Predefined System Properties
 *
 * @author carsten.schipke@gmail.com
 */
@ThreadSafe
public enum SystemProperty
{
  // PREDEFINED SYSTEM PROPERTIES
  COM_THENETCIRCLE_JAVA_PROJECT_TEMPLATE_APP("net.evalcode.java.template.app",
    Standalone.class.getName()
  );

  // MEMBERS
  private final String key;
  private final String defaultValue;


  // CONSTRUCTION
  private SystemProperty(final String key, final String defaultValue)
  {
    this.key=key;
    this.defaultValue=defaultValue;
  }


  // ACCESSORS/MUTATORS
  public String key()
  {
    return key;
  }
  public String value()
  {
    return System.getProperty(key, defaultValue);
  }
  public String rawValue()
  {
    return System.getProperty(key, null);
  }
  public String defaultValue()
  {
    return defaultValue;
  }

  public Integer toInteger()
  {
    return Integer.valueOf(value());
  }
  public Boolean toBoolean()
  {
    return Boolean.valueOf(value());
  }


  // OVERRIDES/IMPLEMENTS
  @Override
  public String toString()
  {
    return value();
  }
}
